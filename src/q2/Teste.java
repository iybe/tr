package q2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Teste {

	public static void main(String[] args) {
//		testeLerTCP();
//		testeLerConsole();
		testeLerArquivo();
	}
	
	private static void testeLerTCP() {
		try {
			int serverPort = 7896; // the server port
			ServerSocket listenSocket = new ServerSocket(serverPort);
			System.out.println("Input ligado");
			while (true) {
				Socket clientSocket = listenSocket.accept();
				System.out.println("Recebi conexao");
				PessoasInputStream pessoaIS = new PessoasInputStream(clientSocket.getInputStream());
				pessoaIS.lerTCP();
			}
		} catch (IOException e) {
			System.out.println("Listen socket:" + e.getMessage());
		}
	}
	
	private static void testeLerConsole() {
		PessoasInputStream pessoaIS = new PessoasInputStream(System.in);
		pessoaIS.lerConsole();
	}

	private static void testeLerArquivo() {
		try {
			File arq = new File("arquivo.txt");
			PessoasInputStream pessoaIS = new PessoasInputStream(new FileInputStream(arq));
			pessoaIS.lerArquivo();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
