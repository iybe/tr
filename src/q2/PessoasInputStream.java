package q2;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Scanner;

import q1.Pessoa;

public class PessoasInputStream extends InputStream {

	private InputStream origem;
	
	public PessoasInputStream(InputStream origem) {
		this.origem = origem;
	}
	
	public void lerConsole() {
		ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
		Scanner ler = new Scanner(this.origem);
		System.out.printf("Informe a quantidade de pessoas: ");
		int qtd = ler.nextInt();
		for(int i = 0; i < qtd; i++) {
			System.out.printf("Nome: ");
			String nome = ler.next();
			System.out.printf("Idade: ");
			int idade = ler.nextInt();
			System.out.printf("Cpf: ");
			int cpf = ler.nextInt();
			Pessoa p = new Pessoa(nome,idade,cpf);
			pessoas.add(p);
		}
	}
	
	public void lerTCP() {
		DataInputStream in = new DataInputStream(this.origem);
		ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
		
		try {
			int tamanhoArray = Integer.parseInt(in.readUTF());
			for(int i = 0; i < tamanhoArray; i++) {
				String qtdBytes = in.readUTF();
				String nome = in.readUTF();
				int idade = Integer.parseInt(in.readUTF());
				int cpf = Integer.parseInt(in.readUTF());
				Pessoa p = new Pessoa(nome,idade,cpf);
				pessoas.add(p);
				System.out.println(qtdBytes+" "+nome+" "+idade+" "+cpf);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	public void lerArquivo() throws FileNotFoundException, IOException {
		try {
			ObjectInputStream objInput = new ObjectInputStream(this.origem);
			ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
			
			int qtd = Integer.parseInt(objInput.readLine());
			for(int i = 0; i < qtd; i++) {
				objInput.readLine();
				String nome = objInput.readLine();
				int idade = Integer.parseInt(objInput.readLine());
				int cpf = Integer.parseInt(objInput.readLine());
//				System.out.println(nome+" "+idade+" "+cpf);
				Pessoa p = new Pessoa(nome,idade,cpf);
				pessoas.add(p);
			}

			objInput.close();
		} catch (IOException erro1) {
			System.out.printf("Erro: %s", erro1.getMessage());
		}
	}
	
	@Override
	public int read() throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

}
