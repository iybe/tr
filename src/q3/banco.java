package q3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class banco {
	private ArrayList<conta> contas;
	
	public banco() {
		this.contas = new ArrayList<conta>();
		carregarContasArquivo("banco.txt");
	}
	
	public void adicionarConta(conta c) {
		this.getContas().add(c);
		gravarContasArquivo("banco.txt");
	}
	
	public boolean excluirConta(int id) {
		try {
			contas.remove(id);
			gravarContasArquivo("banco.txt");
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void gravarContasArquivo(String nomeArquivo) {
		File arq = new File(nomeArquivo);
		arq.delete();
		try {
			arq.createNewFile();
			OutputStream destino = new FileOutputStream(arq);
			ObjectOutputStream objOutput = new ObjectOutputStream(destino);
			objOutput.writeBytes(String.valueOf(getContas().size())+"\n");
			
			for (conta Conta : getContas()) {
				objOutput.writeBytes(Conta.nomeCliente+'\n');
				objOutput.writeBytes(String.valueOf(Conta.id)+'\n');
				objOutput.writeBytes(String.valueOf(Conta.saldo)+'\n');
			}
			
			objOutput.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	public void carregarContasArquivo(String nomeArquivo) {
		try {
			File arq = new File(nomeArquivo);
			if(!arq.exists()) {
				arq.createNewFile();
				OutputStream destino = new FileOutputStream(arq);
				ObjectOutputStream objOutput = new ObjectOutputStream(destino);
				objOutput.writeBytes(String.valueOf(0)+"\n");
				objOutput.close();
				return;
			}
			ObjectInputStream objInput = new ObjectInputStream(new FileInputStream(arq));
			
			int qtd = Integer.parseInt(objInput.readLine());
			for(int i = 0; i < qtd; i++) {
				String nomeCliente = objInput.readLine();
				int id = Integer.parseInt(objInput.readLine());
				double saldo = Double.parseDouble(objInput.readLine());
				conta c = new conta(nomeCliente,id,saldo);
				getContas().add(c);
			}

			objInput.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public ArrayList<conta> getContas() {
		return contas;
	}

	public void exibirContas() {
		for (conta conta : contas) {
			System.out.println(conta.nomeCliente+" "+conta.id+" "+conta.saldo);
		}
	}
	
	public int getId() {
		if(contas.isEmpty()) {
			return 0;
		}
		return contas.get(contas.size()-1).id+1;
	}
}
