package q3;

import java.util.Scanner;

public class main {
	
	public static void main(String[] args) {
		banco Banco = new banco();
		Scanner ler = new Scanner(System.in);
		
		while(true) {
			System.out.printf("> ");
			String comando = ler.nextLine();
			if(comando.isEmpty()) {
				comando = ler.nextLine();
			}
			if(comando.equals("add")) {
				System.out.printf("add-nome > ");
				String nome = ler.nextLine();
				System.out.printf("add-saldo > ");
				double saldo = ler.nextDouble();
				Banco.adicionarConta(new conta(nome,Banco.getId(),saldo));
				System.out.printf("add > CONTA ADICIONADA\n");
			}else if(comando.equals("remove")) {
				System.out.printf("remove-id > ");
				int id = ler.nextInt();
				boolean status = Banco.excluirConta(id);
				if(status) {
					System.out.println("remove > CONTA REMOVIDA");
				}else {
					System.out.println("remove > ERRO AO REMOVER");
				}
			}else if(comando.equals("contas")) {
				System.out.println("contas > --- EXIBINDO CONTAS --- ");
				Banco.exibirContas();
				System.out.println("contas > ----------------------- ");
			}else if(comando.equals("sair")) {
				break;
			}
		}
	}

}
