package q1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Teste {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		testeGravacaoArquivo();
//		testeEnvioConsole();
//		testeEnviarTCP();
	}

	private static void testeGravacaoArquivo() throws IOException{
		Pessoa a = new Pessoa("pA",18,123456789);
		Pessoa b = new Pessoa("pB",18,123456789);
		ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
		pessoas.add(a);
		pessoas.add(b);
		
		File arq = new File("arquivo.txt");
		arq.delete();
		arq.createNewFile();
		OutputStream destino = new FileOutputStream(arq);
		
		@SuppressWarnings("resource")
		PessoasOutputStream pessoasOS = new PessoasOutputStream(pessoas, destino);
		pessoasOS.gravarArquivo();
	}

	private static void testeEnvioConsole() {
		Pessoa a = new Pessoa("pA",18,123456789);
		Pessoa b = new Pessoa("pB",18,123456789);
		ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
		pessoas.add(a);
		pessoas.add(b);
		
		OutputStream destino = System.out;
		PessoasOutputStream pessoasOS = new PessoasOutputStream(pessoas, destino);
		pessoasOS.enviarConsole();
	}
	
	private static void testeEnviarTCP() {
		Pessoa a = new Pessoa("pA",18,123456789);
		Pessoa b = new Pessoa("pB",18,123456789);
		ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
		pessoas.add(a);
		pessoas.add(b);
		
		Socket s = null;
		try {
			int serverPort = 7896;
			s = new Socket("localhost", serverPort);
			PessoasOutputStream pessoaOS = new PessoasOutputStream(pessoas, s.getOutputStream());
			pessoaOS.enviarTCP();
		} catch (UnknownHostException e) {
			System.out.println("Socket:" + e.getMessage());
		} catch (EOFException e) {
			System.out.println("EOF:" + e.getMessage());
		} catch (IOException e) {
			System.out.println("readline:" + e.getMessage());
		} finally {
			if (s != null)
				try {
					s.close();
				} catch (IOException e) {
					System.out.println("close:" + e.getMessage());
				}
		}
	}
}
