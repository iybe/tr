package q1;

public class Pessoa {
	public String nome;
	public int cpf;
	public int idade;
	
	public Pessoa(String nome, int idade, int cpf) {
		this.nome = nome;
		this.idade = idade;
		this.cpf = cpf;
	}
}
