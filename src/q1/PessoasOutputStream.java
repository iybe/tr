package q1;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class PessoasOutputStream extends OutputStream {
	
	public OutputStream destino;
	public ArrayList<Pessoa> pessoas;
	
	public PessoasOutputStream(ArrayList<Pessoa> pessoas, OutputStream destino) {
		this.destino = destino;
		this.pessoas = pessoas;
	}
	
	public void gravarArquivo() throws IOException {
		ObjectOutputStream objOutput = new ObjectOutputStream(this.destino);
		objOutput.writeBytes(String.valueOf(pessoas.size())+"\n");
		
		for (Pessoa pessoa : pessoas) {
			objOutput.writeBytes(String.valueOf(pessoa.nome.getBytes().length)+'\n');
			objOutput.writeBytes(pessoa.nome+'\n');
			objOutput.writeBytes(String.valueOf(pessoa.idade)+'\n');
			objOutput.writeBytes(String.valueOf(pessoa.cpf)+'\n');
		}
		
		objOutput.close();
	}
		
	public void enviarConsole() {
		System.out.println("Quantidade de pessoas: "+pessoas.size());
		for (Pessoa pessoa : pessoas) {
			System.out.println("Quantidade de bytes do nome: "+pessoa.nome.getBytes().length);
			System.out.println("Nome: "+pessoa.nome);
			System.out.println("Idade: "+pessoa.idade);
			System.out.println("Cpf: "+pessoa.cpf);
		}
	}
	
	public void enviarTCP() {
		DataOutputStream out = new DataOutputStream(destino);
		try {
			out.writeUTF(String.valueOf(pessoas.size()));
			for (Pessoa pessoa : pessoas) {
				out.writeUTF(String.valueOf(pessoa.nome.getBytes().length));
				out.writeUTF(String.valueOf(pessoa.nome));
				out.writeUTF(String.valueOf(pessoa.idade));
				out.writeUTF(String.valueOf(pessoa.cpf));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void write(int b) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
